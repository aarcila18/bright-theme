<?php

// add_action( 'get_discount_percentage', 'get_discount_percentage', 10, 2 );

// function get_discount_percentage( $id ){
// 	$product = wc_get_product($id);
// 	$variation_price = $product->get_variation_regular_price( 'min');
// 	$variation_sale_price = $product->get_variation_sale_price( 'min');
// 	$percentage = 100-(($variation_sale_price/$variation_price)*100);
// 	$percentage = round($percentage, 0);
// 	echo $percentage."%";
// }

// add_action( 'get_variation_regular_price', 'get_variation_regular_price', 10, 2 );

// function get_variation_regular_price( $id ){
// 	$product = wc_get_product($id);
// 	$min_variation_price = $product->get_variation_regular_price( 'min');
// 	$price = apply_filters( 'woocs_exchange_value', $min_variation_price );
// 	echo number_format($price, 0, ',', '.');
// }

// add_action( 'get_variation_sale_price', 'get_variation_sale_price', 10, 2 );

// function get_variation_sale_price( $id ){
// 	$product = wc_get_product($id);
// 	$min_variation_price = $product->get_variation_sale_price( 'min');
// 	$price = apply_filters( 'woocs_exchange_value', $min_variation_price );
// 	echo number_format($price, 0, ',', '.');
// }


	
add_filter( 'timber/twig', 'add_to_twig' );

/**
 * My custom Twig functionality.
 *
 * @param \Twig\Environment $twig
 * @return \Twig\Environment
 */
function add_to_twig( $twig ) {
    // Adding a function.
    $twig->addFunction( new Timber\Twig_Function( 'get_product_gallery', 'get_product_gallery' ) );
    
    return $twig;
}

add_action('wp_head', 'myplugin_ajaxurl');

function myplugin_ajaxurl() {

   echo '<script type="text/javascript">
           var ajaxurl = "' . admin_url('admin-ajax.php') . '";
         </script>';
}

add_action( 'render_remarketing_product', 'render_remarketing_product' );
add_action( 'wp_ajax_render_remarketing_product','render_remarketing_product' );
add_action( 'wp_ajax_nopriv_render_remarketing_product', 'render_remarketing_product'); 

function render_remarketing_product() {

	$productos_vistos = explode( ',', $_POST["productos_vistos"] );

	$args = array(
		'post_type' => 'product',
		'stock' => 1,
		'orderby' =>'date',
		'order' => 'DESC',
		'post__in' => $productos_vistos,
	);
	$products = new Timber\PostQuery($args);

	Timber::render( 'template_parts/products_array.twig',["products"=>$products,"activar_tienda"=>get_field('activar_tienda', 'option')]);

	die();

}

add_action( 'render_cart', 'render_cart' );
add_action( 'wp_ajax_render_cart','render_cart' );
add_action( 'wp_ajax_nopriv_render_cart', 'render_cart'); 

function render_cart() {

	foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
		$_product   = apply_filters('woocommerce_cart_item_product',    $cart_item['data'],       $cart_item, $cart_item_key);
		$product_id = apply_filters('woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key);
		$quantity = false;
		if ($_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters('woocommerce_cart_item_visible', true, $cart_item, $cart_item_key)) {
			if (!$_product->is_sold_individually()) {
				$quantity = [
				'name'  => "cart[{$cart_item_key}][qty]",
				'value' => $cart_item['quantity'],
				'min'   => '0',
				'max'   => $_product->backorders_allowed() ? '' : $_product->get_stock_quantity()
				];
			}
		}
		$cart_products[] = [
		'title'     => apply_filters('woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key),
		'link'     => $_product->get_permalink(),
		'image'     => wp_get_attachment_url( $_product->get_image_id() ),
		'remove_url' => WC()->cart->get_remove_url($cart_item_key),
		'price'     => apply_filters('woocommerce_cart_item_price', WC()->cart->get_product_price($_product), $cart_item, $cart_item_key),
		'quantity'  => $quantity,
		'subtotal'  => apply_filters('woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal($_product, $cart_item['quantity']), $cart_item, $cart_item_key)
		];
	}

	

	Timber::render( 'template_parts/cart_template.twig',["cart_products"=>$cart_products]);

	die();

}

add_action( 'render_product_add_to_cart', 'render_product_add_to_cart' );
add_action( 'wp_ajax_render_product_add_to_cart','render_product_add_to_cart' );
add_action( 'wp_ajax_nopriv_render_product_add_to_cart', 'render_product_add_to_cart'); 

function render_product_add_to_cart() {

	$id = $_POST["post_id"];
	$post = new TimberPost($id);
	$product = get_product( $id );

	 // attributes and variations
	 if ( $product->is_type( 'variable' ) ) {

        $product_attributes = $product->get_attributes();
        $product_variation = $product->get_variation_attributes();
        $variation_prices = $product->get_variation_prices()["price"];


        $children = $product->get_children( $args = '', $output = OBJECT ); 
        $variations_attributes = array();

        foreach ($product_attributes as $attribute) {

            $variations_attributes[$attribute["name"]] = array('title' => $attribute["name"],'variation' => []);

            foreach ($children as $key=>$value) {
                $product_variatons = new WC_Product_Variation($value);

                if ( $product_variatons->exists() && $product_variatons->variation_is_visible() ) {
                    $variations[$value] = $product_variatons->get_variation_attributes();

                    // var_dump($attribute["name"]);
                    // die();

                    array_push($variations_attributes[$attribute["name"]]["variation"], array(
                        'name'    => $variations[$value]["attribute_".$attribute["name"]],
                        'stock'   => $product_variatons->get_stock_quantity(),
                        'price'   => $variation_prices[$value]
                        ));
                }
            }

        }

    }


	Timber::render( 'template_parts/add_to_cart_form.twig',["variations_attributes"=>$variations_attributes,"post"=>$post,"activar_tienda"=>get_field('activar_tienda', 'option')]);

	die();
}

function get_product_gallery( $id ){
	$product = wc_get_product($id);
	$attachment_ids = $product->get_gallery_attachment_ids();
    $product_gallery = array();
    foreach( $attachment_ids as $attachment_id ) 
    {
        $image_link = wp_get_attachment_url( $attachment_id );
        array_push($product_gallery, $image_link);
    }

	return $product_gallery;
}


add_action( 'get_stock_sizes', 'get_stock_sizes', 10, 2 );

function get_stock_sizes( $id ){
	$product = get_product( $id );

	$variations_attributes = "Talla unica";

	if ( $product->is_type( 'variable' ) ) {

		$product_attributes = $product->get_attributes();
		$product_variation = $product->get_variation_attributes();

		$children = $product->get_children( $args = '', $output = OBJECT ); 
		$variations_attributes = "";

		foreach ($product_attributes as $attribute) {



			foreach ($children as $key=>$value) {
				$product_variatons = new WC_Product_Variation($value);

				if ( $product_variatons->exists() && $product_variatons->variation_is_visible() ) {
					$variations[$value] = $product_variatons->get_variation_attributes();
					if($product_variatons->get_stock_quantity()>0){

					$variations_attributes = $variations_attributes.$variations[$value]["attribute_".$attribute["name"]]." ";
					$variations_attributes = strtoupper($variations_attributes);
					}

				}
			}

		}



	}

	echo $variations_attributes;
}

add_action( 'get_discount_percentage', 'get_discount_percentage', 10, 2 );

function get_discount_percentage( $id ){
	$product = wc_get_product($id);
	if( $product->is_type( 'simple' ) ) {
		$price = $product->get_regular_price( 'min');
		$sale_price = $product->get_sale_price( 'min');
	} else {
		$price = $product->get_variation_regular_price( 'min');
		$sale_price = $product->get_variation_sale_price( 'min');
	}
	$percentage = 100-(($sale_price/$price)*100);
	$percentage = round($percentage, 0);
	echo $percentage."%";
}

add_action( 'get_regular_price', 'get_regular_price', 10, 2 );

function get_regular_price( $id ){
	$product = wc_get_product($id);
	if( $product->is_type( 'simple' ) ) {
		$min_price = $product->get_regular_price( 'min');
	} else {
		$min_price = $product->get_variation_regular_price( 'min');
	}
	$price = apply_filters( 'woocs_exchange_value', $min_price );
	echo number_format($price, 0, ',', '.');
}

add_action( 'get_sale_price', 'get_sale_price', 10, 2 );

function get_sale_price( $id ){
	$product = wc_get_product($id);
	if( $product->is_type( 'simple' ) ) {
		$min_price = $product->get_sale_price( 'min');
	} else {
		$min_price = $product->get_variation_sale_price( 'min');
	}
	$price = apply_filters( 'woocs_exchange_value', $min_price );
	echo number_format($price, 0, ',', '.');
}

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Ajustes BYP',
		'menu_title'	=> 'Ajustes BYP',
		'menu_slug' 	=> 'ajustes_byp',
		'redirect'		=> false
		));
}

// routes 

add_action('get_cat_image', 'get_cat_image_action');

function get_cat_image_action($id){
	$thumbnail_id = get_woocommerce_term_meta( $id, 'thumbnail_id', true );
	$image = wp_get_attachment_url( $thumbnail_id );
	echo $image;
}


add_action('get_cat_link', 'get_cat_link');

function get_cat_link($id){
	$link = get_category_link( $id );
	echo $link;
}

add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
	add_theme_support( 'woocommerce' );
}

function timber_set_product( $post ) {
	global $product;
	if ( is_woocommerce() ) {
		$product = get_product($post->ID);
	}
}

if ( ! class_exists( 'Timber' ) ) {
	add_action( 'admin_notices', function() {
		echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php' ) ) . '</a></p></div>';
	} );
	return;
}

function shortcode_my_orders( $atts ) {
	extract( shortcode_atts( array(
		'order_count' => -1
		), $atts ) );

	ob_start();
	wc_get_template( 'myaccount/my-orders.php', array(
		'current_user'  => get_user_by( 'id', get_current_user_id() ),
		'order_count'   => $order_count
		) );
	return ob_get_clean();
}
add_shortcode('my_orders', 'shortcode_my_orders');

Timber::$dirname = array('templates', 'views');

class StarterSite extends TimberSite {

	function __construct() {
		add_theme_support( 'post-formats' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'menus' );
		add_filter( 'timber_context', array( $this, 'add_to_context' ) );
		add_filter( 'get_twig', array( $this, 'add_to_twig' ) );
		add_action( 'init', array( $this, 'register_post_types' ) );
		add_action( 'init', array( $this, 'register_taxonomies' ) );
		parent::__construct();
	}

	function register_post_types() {
		//this is where you can register custom post types
	}

	function register_taxonomies() {
		//this is where you can register custom taxonomies
	}

	function add_to_context( $context ) {
		$context['version_cache'] = "250520201";
		$context['menu'] = new TimberMenu("menu principal");
		$context['menu_cat'] = new TimberMenu("categorias");
		$context['menu_horizontal'] = new TimberMenu("menu horizontal");
		$context['site'] = $this;
		$context['public_folder'] = $context['site']->theme->link . "/public";

		$context['cart_empty'] = WC()->cart->is_empty();
		$context['cart_url'] = WC()->cart->get_cart_url();
		$context['shop_url'] = wc_get_page_permalink('shop');
		$context['checkout_url'] = WC()->cart->get_checkout_url();
		// $context['cart_products'] = $cart_products;
		$context['cart_count'] = WC()->cart->get_cart_contents_count();
		$context['cart_total'] = WC()->cart->total;

		$context['product_categories'] = Timber::get_terms('product_cat', array('parent' => 0));
		
		// $products_args = array(
		// 	'post_type' => 'product',
		// 	'stock' => 1,
		// 	'posts_per_page' => -1, 
		// 	'orderby' =>'date',
		// 	'order' => 'DESC' 
		// 	);

		// $context['products'] = Timber::get_posts($products_args);

		// $products_in_sale_args = array(
		// 	'post_type' => 'product',
		// 	'stock' => 1,
		// 	'relation' => 'AND',
		// 	'meta_query' => array(
		// 		array(
		// 			'key'     => '_sale_price',
		// 			'value'   => null,
		// 			'compare' => '!='
		// 			)
		// 		)

		// 	);

		// $context['products_in_sale'] = Timber::get_posts($products_in_sale_args);

		// $products_newones_args = array(
		// 	'post_type' => 'product',
		// 	'stock' => 1,
		// 	'posts_per_page' => 5, 
		// 	'orderby' =>'date',
		// 	'order' => 'DESC' 
		// 	);

		// $context['products_newones'] = Timber::get_posts($products_newones_args);

		// esto es para obtener todo de qtranslate
		// $context["enabled_languages"] = get_option('qtranslate_enabled_languages');
		//$context["languages"] = array_combine(get_option('qtranslate_enabled_languages'),get_option('qtranslate_language_names'));
		// $context["current_language"] = qtrans_getLanguage();
		global $current_user;
		get_currentuserinfo();
		$context["current_user"] = $current_user;
		$context["username"] = $current_user->display_name;
		$context["logout_url"] = wp_logout_url( home_url() );
		global $WOOCS;
		$currencies=$WOOCS->get_currencies();
		$context["current_currency"] = $WOOCS->current_currency;
		$context["current_currency_obj"] = $currencies[$WOOCS->current_currency];
		global $wp;
		$context["current_url"] = home_url(add_query_arg(array(),$wp->request))."/";
		$context['redes'] = get_field('redes', 'option');
		$context['activar_tienda'] = get_field('activar_tienda', 'option');
		$context['subheader_text'] = get_field('subheader_text', 'option');
		$context['actual_page'] = get_query_var('pagename'); 

		$context['universal_logo'] = get_field('logo', 'option');

		return $context;
	}

	function myfoo( $text ) {
		$text .= ' bar!';
		return $text;
	}

	function add_to_twig( $twig ) {
		/* this is where you can add your own fuctions to twig */
		$twig->addExtension( new Twig_Extension_StringLoader() );
		$twig->addFilter('myfoo', new Twig_SimpleFilter('myfoo', array($this, 'myfoo')));
		return $twig;
	}

}

new StarterSite();

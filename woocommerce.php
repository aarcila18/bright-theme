<?php

$context            = Timber::get_context();
$context['sidebar'] = Timber::get_widgets('shop-sidebar');
$post = new TimberPost();
$context['post'] = $post;
$context['size_attributes'] = get_terms("pa_size");


if (is_singular('product')) {

    $context["titulo"] = $post->title." - Bright - Compra en linea - 100% Colombiano";
    $product            = get_product( $context['post']->ID );
    $product_wc = new WC_Product($product->ID );
    $context['product'] = $product;

    

    //gallery

    $attachment_ids = $product->get_gallery_attachment_ids();
    $context['product_gallery'] = array();
    foreach( $attachment_ids as $attachment_id ) 
    {
        $image_link = wp_get_attachment_url( $attachment_id );
        array_push($context['product_gallery'], $image_link);
    }

    // stars system
    $context["kk_star"] = kk_star_ratings($context['post']->ID);

    // attributes and variations
    if ( $product->is_type( 'variable' ) ) {

        $product_attributes = $product->get_attributes();
        $product_variation = $product->get_variation_attributes();
        $variation_prices = $product->get_variation_prices()["price"];


        $children = $product->get_children( $args = '', $output = OBJECT ); 
        $variations_attributes = array();

        foreach ($product_attributes as $attribute) {

            $variations_attributes[$attribute["name"]] = array('title' => $attribute["name"],'variation' => []);

            foreach ($children as $key=>$value) {
                $product_variatons = new WC_Product_Variation($value);

                if ( $product_variatons->exists() && $product_variatons->variation_is_visible() ) {
                    $variations[$value] = $product_variatons->get_variation_attributes();

                    // var_dump($attribute["name"]);
                    // die();

                    array_push($variations_attributes[$attribute["name"]]["variation"], array(
                        'name'    => $variations[$value]["attribute_".$attribute["name"]],
                        'stock'   => $product_variatons->get_stock_quantity(),
                        'price'   => $variation_prices[$value]
                        ));
                }
            }

        }


        $context["variations_attributes"] = $variations_attributes;
    }

    // cross sells

    $crosssell_ids = get_post_meta( get_the_ID(), '_crosssell_ids' ); 
    $crosssell_ids=$crosssell_ids[0];
    $crosssells = array();

    if(count($crosssell_ids)>0){
        $crosssell_args = array( 'post_type' => 'product', 'posts_per_page' => 10, 'post__in' => $crosssell_ids );
        $crosssell = new WP_Query( $crosssell_args );
        while ( $crosssell->have_posts() ) {
            $crosssell->the_post();
            array_push($crosssells, new TimberPost(get_post()->ID));
        }
    }

    $context['crosssell_products'] = $crosssells;



    Timber::render('views/woo/single-product.twig', $context);

} else {

    if ( is_product_category() || is_product_tag() ) {
        $queried_object = get_queried_object();
        $term_id = $queried_object->term_id;
        $context['category_id'] = $term_id;
        $context['category'] = get_term( $term_id, 'product_cat' );
        $context['title'] = single_term_title('', false);
        $context["titulo"] = single_term_title('', false)." - Bright - Compra en linea - 100% Colombiano";

        Timber::render('views/woo/query-products.twig', $context);
    }else{

        $post = new TimberPost(19);
        $context['post'] = $post;


        global $paged;
        if (!isset($paged) || !$paged){
            $paged = 1;
        }

        $context["titulo"] = "Tienda Online - Bright - Compra en linea - 100% Colombiano";

          $products_args = array(
            'post_type' => 'product',
            'stock' => 1,
            's' => $_GET['search'],
            'posts_per_page' => 20, 
            'orderby' =>'date',
            'paged' => $paged,
            'order' => 'DESC',
            );

            if ( $_GET['size_filter'] != null ) {
                $products_args = array(
                    'post_type' => array('product', 'product_variation'),
                    's' => $_GET['search'],
                    'posts_per_page' => 20, 
                    'orderby' =>'date',
                    'paged' => $paged,
                    'order' => 'DESC',
                    'tax_query' => array(
                        array(
                           'taxonomy' => 'pa_size',
                           'field' => 'slug',
                           'terms' => $_GET['size_filter']
                        )
                        ),
                    'meta_query' => array(
                            array(
                            'key' => '_stock_status',
                            'value' => 'instock',
                            'compare' => '='
                            ),
                         )
                    );
            }

        $context['products'] = new Timber\PostQuery($products_args);

        Timber::render('views/woo/all-products.twig', $context);

    }

    

}
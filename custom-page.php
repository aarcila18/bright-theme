<?php
/*
 * Template Name: Byp Custom Template
 * Description: 
 */


$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;
$context["titulo"] = $post->post_name." - Bright - Compra en linea - 100% Colombiano";

// if ($post->maquetacion["acf_fc_layout"] == "productos") {
	
// }

foreach (get_field( "maquetacion" ) as $key => $value) {

	if ($value["acf_fc_layout"] == "productos") {
		$products_args = array(
			'post_type' => 'product',
			'stock' => 1,
			'posts_per_page' => -1, 
			'orderby' =>'date',
			'order' => 'DESC' 
			);

		$context['products'] = Timber::get_posts($products_args);
	}
	
}
Timber::render( "module-page.twig", $context );
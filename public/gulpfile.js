(function () {
    'use strict';

    var gulp    = require('gulp');
    var uglify  = require('gulp-uglify');
    var sass    = require('gulp-sass');
    var plumber = require('gulp-plumber');
    var imageop = require('gulp-image-optimization');
    var livereload = require('gulp-livereload');

    // minify javascripts
    gulp.task('scripts', function (){
        console.log('Minifying your JS...');

        gulp.src('js/*.js')
        .pipe(plumber())
        .pipe(uglify())
        .pipe(gulp.dest('build/js'))
        .pipe(livereload({ start: true }));
    });

    // minify images
    gulp.task('images', function(cb) {
        gulp.src(['images/**/*.png','images/**/*.jpg','images/**/*.gif','images/**/*.jpeg,images/**/*.PNG','images/**/*.JPG','images/**/*.GIF','images/**/*.JPEG']).pipe(imageop({
            optimizationLevel: 5,
            progressive: true,
            interlaced: true
        })).pipe(gulp.dest('images/')).on('end', cb).on('error', cb);
    });

    // watch sass files
    gulp.task('styles', function () {
        console.log('Sass is compiling...');

        gulp.src('sass/**/*.scss')
        .pipe(plumber())
        .pipe(sass({
            style: 'compressed'
        }))
        .pipe(gulp.dest('build/css'))
        .pipe(livereload({ start: true }));
    });

    // watch sass files
    gulp.task('twigs', function () {
        gulp.src('../views/**/*.twig')
        .pipe(livereload({ start: true }));
    });

    // watch task to check files for changes
    gulp.task('watch', function () {
        livereload.listen();
        gulp.watch('js/*.js', ['scripts']);
        gulp.watch('sass/**/*.scss', ['styles']);
        gulp.watch('../views/**/*.twig', ['twigs']);
    });

    gulp.task('render', ['scripts', 'styles', 'twigs', 'watch']);

}());
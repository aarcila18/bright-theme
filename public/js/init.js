function pageIniApp() {

    var swiper = new Swiper('.swiper-product', {
        lazy: true,
        pagination: {
            el: '.swiper-pagination',
            type: 'bullet'
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });


    $('.add_to_cart_produts').click(function() {
        $("#modal-add-to-cart .modal-content").html('<h5 class="center-align animated infinite pulse">Cargando el producto ...</h5>');
        var id = $(this).attr("add-to-cart-data");
        $.ajax({
            type: "POST",
            url: "/wp-admin/admin-ajax.php",
            data: {
                action: 'render_product_add_to_cart',
                post_id: id
            },
            success: function(data) {
                console.log(data);
                $("#modal-add-to-cart .modal-content").html(data);


            }
        });
    });

    $('.icon-shop').click(function() {
        $("#modal-cart .modal-content").html('<h5 class="center-align animated infinite pulse">Cargando el carrito ...</h5>');

        $.ajax({
            type: "POST",
            url: "/wp-admin/admin-ajax.php",
            data: {
                action: 'render_cart',
            },
            success: function(data) {
                console.log(data);
                $("#modal-cart .modal-content").html(data);


            }
        });
    });

    $('.menu-activator').click(function() {
        $("#sub-nav").addClass("sub-hidden");
        $("#top-bar").addClass("sub-hidden");
        $('.sublevel-activator-1').removeClass("active");
        $('.sublevel-activator-2').removeClass("active");
        $('.sublevel-activator-3').removeClass("active");
        $('.mobile-sublevel-activator-1').removeClass("active");
        $('.mobile-sublevel-activator-2').removeClass("active");
        $('.mobile-sublevel-activator-3').removeClass("active");

        if ($('.menu-activator').hasClass('open')) {
            $("#sub-nav").removeClass("sub-hidden");
            $("#top-bar").removeClass("sub-hidden");
            $("#menu-category-preview").slideUp(500, "easeOutCirc", function() {});
        }
        // $( "#menu-container" ).animate({ height: 'toggle' }, 1000 , "easeOutCirc", function(){
        //   $('.menu-activator').toggleClass('open');

        //       $( ".sublevel-2" ).hide();
        //       $( ".sublevel-3" ).hide();
        //       $( ".sublevel-4" ).hide();
        // });
        $("#menu-container").slideToggle(1000, "easeOutCirc", function() {
            $('.menu-activator').toggleClass('open');

            $(".sublevel-2").hide();
            $(".sublevel-3").hide();
            $(".sublevel-4").hide();
        });
    });

    $('.sublevel-activator-1').click(function() {
        var sub_id = $(this).attr("data-sublevel-1");
        $('.sublevel-activator-1').removeClass("active");
        $('.sublevel-activator-2').removeClass("active");
        $('.sublevel-activator-3').removeClass("active");
        $(this).addClass("active");
        $(".sublevel-2").hide();
        $(".sublevel-3").hide();
        $(".sublevel-4").hide();
        $("#" + sub_id).slideToggle("slow", function() {

        });
    });
    $('.sublevel-activator-2').click(function() {
        var sub_id = $(this).attr("data-sublevel-2");
        $('.sublevel-activator-2').removeClass("active");
        $('.sublevel-activator-3').removeClass("active");
        $(this).addClass("active");
        $(".sublevel-3").hide();
        $(".sublevel-4").hide();
        $("#" + sub_id).slideToggle("slow", function() {

        });
    });
    $('.sublevel-activator-3').click(function() {
        var sub_id = $(this).attr("data-sublevel-3");
        $('.sublevel-activator-3').removeClass("active");
        $(this).addClass("active");
        $(".sublevel-4").hide();
        $("#" + sub_id).slideToggle("slow", function() {

        });
    });

    $('.mobile-sublevel-activator-1').click(function() {
        var sub_id = $(this).attr("data-mobile-sublevel-1");
        $('.mobile-sublevel-activator-1').removeClass("active");
        $('.mobile-sublevel-activator-2').removeClass("active");
        $('.mobile-sublevel-activator-3').removeClass("active");
        $(this).addClass("active");
        $(".mobile-sublevel-2").hide();
        $(".mobile-sublevel-3").hide();
        $(".mobile-sublevel-4").hide();
        $("#" + sub_id).slideToggle("slow", function() {

        });
    });
    $('.mobile-sublevel-activator-2').click(function() {
        var sub_id = $(this).attr("data-mobile-sublevel-2");
        $('.mobile-sublevel-activator-2').removeClass("active");
        $('.mobile-sublevel-activator-3').removeClass("active");
        $(this).addClass("active");
        $(".mobile-sublevel-3").hide();
        $(".mobile-sublevel-4").hide();
        $("#" + sub_id).slideToggle("slow", function() {

        });
    });
    $('.mobile-sublevel-activator-3').click(function() {
        var sub_id = $(this).attr("data-mobile-sublevel-3");
        $('.mobile-sublevel-activator-3').removeClass("active");
        $(this).addClass("active");
        $(".mobile-sublevel-4").hide();
        $("#" + sub_id).slideToggle("slow", function() {

        });
    });

    $('.category-link').click(function() {
        var height_preview = $(window).height() - 64 - document.getElementById('menu-container').offsetHeight;
        $("#menu-category-preview").height(height_preview);
        var imagen_preview = $(this).attr('data-preview');
        console.log(imagen_preview);
        if (imagen_preview == "") {
            $("#menu-category-preview").slideUp(500, "easeOutCirc", function() {});
        } else {
            $("#menu-category-preview").slideUp(200, "easeOutCirc", function() {
                $('#menu-category-preview').css('background-image', 'url(' + imagen_preview + ')');
            });
            $("#menu-category-preview").slideDown(500, "easeOutCirc", function() {

            });
        }
    });

    $('#dropdown-user').dropdown({
        inDuration: 300,
        outDuration: 225,
        constrain_width: false, // Does not change width of dropdown to that of the activator
        hover: false, // Activate on hover
        gutter: 0, // Spacing from edge
        belowOrigin: true, // Displays dropdown below the button
        alignment: 'right' // Displays dropdown with edge aligned to the left of button
    });

    $('#boton-idiomas').dropdown({
        inDuration: 300,
        outDuration: 225,
        constrain_width: false, // Does not change width of dropdown to that of the activator
        hover: false, // Activate on hover
        gutter: 0, // Spacing from edge
        belowOrigin: true, // Displays dropdown below the button
        alignment: 'right' // Displays dropdown with edge aligned to the left of button
    });

    $('#dropdown-user-mob').dropdown({
        inDuration: 300,
        outDuration: 225,
        constrain_width: false, // Does not change width of dropdown to that of the activator
        hover: false, // Activate on hover
        gutter: 0, // Spacing from edge
        belowOrigin: true, // Displays dropdown below the button
        alignment: 'right' // Displays dropdown with edge aligned to the left of button
    });

    $('#boton-idiomas-mob').dropdown({
        inDuration: 300,
        outDuration: 225,
        constrain_width: false, // Does not change width of dropdown to that of the activator
        hover: false, // Activate on hover
        gutter: 0, // Spacing from edge
        belowOrigin: true, // Displays dropdown below the button
        alignment: 'right' // Displays dropdown with edge aligned to the left of button
    });

    $('.modal-trigger').leanModal();

    // convert select to buttons

    var selectName = $('select.button-style').attr('name');

    // add a hidden element with the same name as the select
    var hidden = $('<input type="hidden" name="' + selectName + '">');
    // hidden.val($('select.button-style').val());
    hidden.insertAfter($('select.button-style'));

    $("select.button-style option").unwrap().each(function() {
        var btn = $('<div follow-help-data="stock: ' + $(this)[0].dataset.stock + '" class="btn-select follow-help">' + $(this).text() + '</div><div style="font-size: 22px;display: inline-block;">/</div>');
        $(this).replaceWith(btn);
    });

    $(document).on('click', '.btn-select', function() {
        $('.btn-select').removeClass('on');
        $(this).addClass('on');
        $('input[name="' + selectName + '"]').val($(this).text());
    });

    $('select').material_select();


    $('.follow-help').hover(
        function(e) {

            var hover_text = $(this).attr("follow-help-data");

            $('#hover_div').addClass('open');
            $('#hover_div').css({
                left: e.pageX + 5,
                top: e.pageY + 10
            });
            $("#hover_div").text(hover_text);

        },
        function(e) {
            $('#hover_div').removeClass('open');
            $('#hover_div').css({
                left: -100,
                top: -100
            });
            $("#hover_div").text("");
        }
    );



    $(".follow-help").mousemove(function(e) {
        var hover_text = $(this).attr("follow-help-data");

        $('#hover_div').css({
            left: e.pageX + 5,
            top: e.pageY + 10
        });
        $("#hover_div").text(hover_text);
    });

    $('.button-collapse').sideNav();
    $('.parallax').parallax();

    $('.dropdown-button').dropdown({
        inDuration: 300,
        outDuration: 225,
        constrain_width: false, // Does not change width of dropdown to that of the activator
        hover: true, // Activate on hover
        gutter: 0, // Spacing from edge
        belowOrigin: true, // Displays dropdown below the button
        alignment: 'left' // Displays dropdown with edge aligned to the left of button
    });


    $('.collapsible').collapsible();

    var lastScroll = 0;
    var down = false;
    var inicialHeight = 60;
    var actualHeight = 60;
    $(window).scroll(function() {


        if ($(this).scrollTop() > 200) { // this refers to window

            var scroll = $(this).scrollTop();
            down = scroll > lastScroll;
            lastScroll = scroll;


            if (down) {

                actualHeight -= 2;

                if (actualHeight < 0) {
                    actualHeight = 0;
                }
                $(".icon-mobile-wrapper").height(actualHeight);

            } else {
                actualHeight += 5;

                if (actualHeight > 60) {
                    actualHeight = 60;
                }
                $(".icon-mobile-wrapper").height(actualHeight);

            }

            $("#sub-nav").addClass("sub-hidden");
            $("#top-bar").addClass("sub-hidden");

            if ($('.menu-activator').hasClass('open')) {
                $("#menu-container").slideUp("slow", function() {
                    actualHeight = 0;
                    $('.menu-activator').removeClass('open');

                    $(".sublevel-2").hide();
                    $(".sublevel-3").hide();
                    $(".sublevel-4").hide();
                });
                $("#menu-category-preview").slideUp(500, "easeOutCirc", function() {});
            }

        } else {
            actualHeight = 60;
            $(".icon-mobile-wrapper").height(actualHeight);

            $("#sub-nav").removeClass("sub-hidden");
            $("#top-bar").removeClass("sub-hidden");
        }
    });
}


(function($) {
    $(function() {

        var reveal_img = {
            origin: 'center',
            delay: 100,
            duration: 500,
            mobile: false,
            reset: false,
            distance: '0%',
            easing: 'ease-in-out',
            scale: 1
        };

        window.sr = ScrollReveal();
        sr.reveal('.reveal_img', reveal_img);
        sr.reveal('.reveal_img_producto', reveal_img, 500);


    }); // end of document ready
})(jQuery); // end of jQuery name space

function add_remarketing_products() {
    var ultimo_producto_visto = localStorage['ultimo_producto_visto'];
    var productos_vistos = localStorage['productos_vistos'];
    if (localStorage['productos_vistos'] != null) {
        console.log(productos_vistos);

        $.ajax({
            type: "POST",
            url: "/wp-admin/admin-ajax.php",
            data: {
                action: 'render_remarketing_product',
                productos_vistos: productos_vistos
            },
            success: function(data) {
                console.log(data);
                $("#remarketing_products").html(data);
                var swiper = new Swiper('.swiper-product', {
                    lazy: true,
                    pagination: {
                        el: '.swiper-pagination',
                        type: 'bullet'
                    },
                    navigation: {
                        nextEl: '.swiper-button-next',
                        prevEl: '.swiper-button-prev',
                    },
                });
            }
        });

    } else {
        console.log("no ha visto productos");
    }

}

$(window).load(function() {
    // Animate loader off screen
    pageIniApp();
    $(".se-pre-con").fadeOut("slow");


});

// $(document).on('page:load', function() {
//     pageIniApp();
// });

// $(document).on('page:fetch', function() {
//     $(".se-pre-con").fadeIn("slow");
// });

// $(document).on('page:change', function() {
//     $(".se-pre-con").fadeOut("slow");
// });